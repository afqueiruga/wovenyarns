Woven Yarns
===========

A multiphysics model of heterogenous textiles and yarns implemented in FEniCS.

Requirements
------------

afq's patches of dolfin and FFC are required, see the corresponding forks. Other custom modules 
for interacting with FEniCS are required and are referenced by this repo as submodules.

The repositories are:

- [https://bitbucket.org/afqueiruga/dolfin-fork](https://bitbucket.org/afqueiruga/dolfin-fork)
- [https://bitbucket.org/afqueiruga/ffc-fork](https://bitbucket.org/afqueiruga/ffc-fork)
- [https://bitbucket.org/afqueiruga/multiwriter](https://bitbucket.org/afqueiruga/multiwriter)
- [https://bitbucket.org/afqueiruga/broadcastassembler](https://bitbucket.org/afqueiruga/broadcastassembler)
- [https://bitbucket.org/afqueiruga/proximitytree](https://bitbucket.org/afqueiruga/proximitytree)

Citations
-------

This repository was the basis of the following works:

- Dissertation: "Microscale Simulation of the Mechanical and 
Electromagnetic Behavior of Textiles"
- **Queiruga, A. F.** and T. Zohdi, “Microscale modeling of effective 
  mechanical and electrical properties of textiles,” 
  International Journal for Numerical Methods in Engineering, 2016.
  doi: 10.1002/nme.5268.
- **Queiruga, A. F.** and T. Zohdi, “Formulation and numerical 
  analysis of a fully-coupled dynamically deforming 
  electromagnetic wire,” Computer Methods in Applied Mechanics 
  and Engineering, vol. 305, pp. 292--315, 2016. doi: 10.1016/j.cma.2016.02.035.

License
-------

Copyright (c) 2014, Alejandro Francisco Queiruga
Department of Mechanical Engineering
University of California, Berkeley

This package is experimental and NOT fit for commercial use.
Research use of this code is encouraged. Please contact the 
author for academic/research use of the code.

The submodules are GPLv2 and may be used, see their corresponding
repositories for usage.
